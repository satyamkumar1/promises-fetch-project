
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/
function fetchUser() {


    const data = fetch('https://jsonplaceholder.typicode.com/users')
        .then((data) => {
            return data.text();
        })
        .then((item) => {
            //console.log(JSON.parse(item));
        })
        .catch((err) => {
            console.log("error" + err)
        });



}
fetchUser();



function fetchTodos() {


    const dataTodos = fetch('https://jsonplaceholder.typicode.com/todos')
        .then((data) => {
            return data.text();
        })
        .then((item) => {
            //console.log(JSON.parse(item));
        })
        .catch((err) => {
            console.log("error" + err)
        });

}
fetchTodos();



function fetchUsersAndTodos() {

    const url = "https://jsonplaceholder.typicode.com";

    const getData = fetch(`${url}${"/users"}`)
        .then((data) => {
            return data.json();
        })
        .then((data) => {
            /// console.log("user-data");
            //console.log(data);

            return fetch(`${url}${"/todos"}`)
                .then((data) => {
                    return data.json();
                })
        })
        .then((item) => {
            //console.log("todos-data");
            // console.log(item);
        })
        .catch((err) => {
            console.log("error" + err)
        });


}
fetchUsersAndTodos();



function fetchUserAndAllUsersData() {

    const fetchData = fetch('https://jsonplaceholder.typicode.com/users')
        .then((data) => {
            return data.json();
        })
        .then((item) => {
            const userData = item.map((data) => {
                return fetch(`https://jsonplaceholder.typicode.com/users/${data.id}`)
                    .then((item) => {
                        return item.json();
                    })
            });

            return Promise.all(userData);


        }).then((data) => {
            console.log(data);
        })
        .catch((err) => {
            console.log("error" + err)
        });


}
fetchUserAndAllUsersData();





